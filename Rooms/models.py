from django.db import models
from Event_and_creator.models import plat_event
from django.db.models import CheckConstraint, Q


# Create your models here.


class plat_rooms_buildings(models.Model):
    id = models.AutoField(primary_key=True)
    name_or_number = models.CharField(max_length=50)
    floors = models.PositiveIntegerField()
    comment = models.TextField(max_length=4000, blank=True, null=True)
    event_id = models.ForeignKey(plat_event, on_delete=models.CASCADE,
                                            verbose_name='Внешний ключ к мероприятию',db_column='event_id')

    class Meta:
        managed = True
        db_table = 'plat_rooms_buildings'
        verbose_name = 'Корпус'
        verbose_name_plural = 'Список корпусов'


class plat_rooms_main(models.Model):
    id = models.AutoField(primary_key=True)
    room_number = models.IntegerField(verbose_name='Номер комнаты')
    room_floor = models.IntegerField(verbose_name='Этаж')
    building = models.ForeignKey('plat_rooms_buildings', on_delete=models.CASCADE, verbose_name='Корпус/Строение')
    bed_num = models.PositiveIntegerField(verbose_name='Количество кроватей заявлено кроватей')
    bed_num_actual = models.PositiveIntegerField(verbose_name='Количество кроватей по факту кроватей')
    description = models.TextField(max_length=4000, blank=True, null=True)
    event_id = models.ForeignKey(plat_event, on_delete=models.CASCADE,
                                 verbose_name='Внешний ключ к мероприятию', db_column='event_id')

    class Meta:
        managed = True
        db_table = 'plat_rooms_main'
        verbose_name = 'Комната'
        verbose_name_plural = 'Комнаты'




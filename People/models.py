from django.db import models
from Rooms.models import plat_rooms_main
from Event_and_creator.models import plat_event


# Create your models here.


class plat_timetable_teams(models.Model):
    name = models.CharField(max_length=40, verbose_name='Название')
    event_id = models.ForeignKey(plat_event, db_column='event_id', on_delete=models.CASCADE, verbose_name='Мероприятие')

    class Meta:
        managed = True
        db_table = 'plat_timetable_teams'
        verbose_name = 'Команда'
        verbose_name_plural = 'Команды'


class plat_people_main(models.Model):  # Люди
    first_name = models.CharField(max_length=20, verbose_name='Имя')
    middle_name = models.CharField(max_length=20, verbose_name='Фамилия')
    family_name = models.CharField(max_length=20, verbose_name='Отчество')
    phone = models.CharField(unique=True, max_length=15, verbose_name='Телефон')
    group_name = models.CharField(max_length=10, blank=True, null=True, verbose_name='Номер группы')
    chat_id = models.DecimalField(max_digits=15, decimal_places=0,  unique=True, null=True, blank=True)
    telegram_username = models.CharField(unique=True, max_length=50, verbose_name='Телеграм id')
    vk_url = models.CharField(max_length=50, verbose_name='VK')

    # Вложенный класс определяет связь между моделью и сущностью внутри базы данных
    class Meta:
        managed = True  # изменения модели влекут за собой изменения сущности в базе данных
        db_table = 'plat_people_main'  # связанная таблица

    # Специальный метод для отображения ФИО целиком
    def i_name(self):
        return self.middle_name + ' ' + self.first_name + ' ' + self.family_name

    # Краткое название метода
    i_name.short_description = 'ФИО'


class plat_people_participants(plat_people_main):  # участник
    choice_status_list_1 = (
        (0, 'Не приехал'),
        (1, 'Приехал'),
        (2, 'Выехал'),
    )
    part_id = models.IntegerField(blank=True, null=True)
    check_in = models.TimeField(blank=True, null=True, verbose_name='Время заезда')
    arrive_status = models.IntegerField(verbose_name='Статус прибытия', choices=choice_status_list_1, default=0)
    room = models.ForeignKey(plat_rooms_main, on_delete=models.CASCADE, blank=True, null=True, verbose_name='Комната')
    team = models.ForeignKey(plat_timetable_teams, on_delete=models.CASCADE, blank=True, null=True, verbose_name='Команда')
    event_id = models.ForeignKey(plat_event, db_column='event_id', on_delete=models.CASCADE)

    def __str__(self):
        return self.family_name + ' ' + self.first_name + ' ' + self.middle_name

    class Meta:
        managed = True
        db_table = 'plat_people_participants'
        verbose_name = 'Участник'
        verbose_name_plural = 'Участники'


class plat_people_org(plat_people_main):  # организатор
    previllage = models.DecimalField(max_digits=2, decimal_places=0,  default=0)
    check_out = models.BooleanField(default=False)
    event_id = models.ForeignKey(plat_event, db_column='event_id', on_delete=models.CASCADE)

    def __str__(self):
        return self.middle_name + ' ' + self.family_name + ' ' + self.first_name

    class Meta:
        managed = True
        db_table = 'plat_people_org'
        verbose_name = 'Организатор'
        verbose_name_plural = 'Организаторы'


class plat_people_on_events(models.Model):
    choice_status_list = (
        (0, 'Без статуса'),
        (1, 'Организатор'),
        (2, 'Участник'),
    )
    person = models.ForeignKey(plat_people_main, on_delete=models.CASCADE)
    event_id = models.ForeignKey(plat_event, db_column='event_id', on_delete=models.CASCADE)
    status = models.SmallIntegerField(default=0, verbose_name='Статус', choices=choice_status_list)

    class Meta:
        managed = True
        db_table = 'plat_event_people'
        verbose_name = 'Человек на мероприятит'
        verbose_name_plural = 'Люди на мероприятиях'


class plat_people_drivers(models.Model):
    choice_status_list = (
        (0, 'Есть машина не готов помочь'),
        (1, 'Есть машина, готов помочь'),
        (2, 'Есть машина, готов помочь, выбран организатором'),
    )
    person = models.ForeignKey(plat_people_main, on_delete=models.CASCADE)
    capacity = models.DecimalField(max_digits=4, decimal_places=2)
    status = models.SmallIntegerField(default=0, verbose_name='Статус', choices=choice_status_list)
    compensation = models.DecimalField(max_digits=4, decimal_places=2, null=True, blank=True)
    commentary = models.TextField(max_length=4000, null=True, blank=True)
    event_id = models.ForeignKey(plat_event, db_column='event_id', on_delete=models.CASCADE)

    class Meta:
        managed = True
        db_table = 'plat_people_drivers'
        verbose_name = 'Водитель, который может помочь'
        verbose_name_plural = 'Водители, которые могут помочь'

from django.db import models
from django.contrib.auth.models import AbstractUser


# Create your models here.


class plat_users(models.Model):

    id = models.AutoField(primary_key=True)
    login = models.CharField(max_length=20)
    password = models.CharField(max_length=20)
    email = models.EmailField()
    #key = models.CharField(max_length=10,default=None)

    class Meta:
        managed = True
        db_table = 'plat_users'
        verbose_name = 'Поьзователь'


class plat_event(models.Model):
    name = models.CharField(max_length=50, verbose_name='Название')
    date_of_create = models.DateTimeField(verbose_name='Дата создания')
    date_of_event = models.DateTimeField(verbose_name='Дата мероприятия', null=True, blank=True,)
    date_of_archive = models.DateTimeField(verbose_name='Дата архивации', null=True, blank=True,)
    creator_id = models.ForeignKey(plat_users, db_column='creator_id', on_delete=models.CASCADE,
                                   verbose_name='Пользователь-создатель')

    class Meta:
        managed = True
        db_table = 'plat_events'
        verbose_name = 'Мероприятие'


class plat_event_location(models.Model):
    choice_status_list = (
        (0, 'Не выбран'),
        (1, 'Выбран'),
    )
    name = models.CharField(max_length=50, verbose_name='Название')
    website = models.CharField(max_length=50, null=True, blank=True, verbose_name='Сайт')
    adress = models.CharField(max_length=100, null=True, blank=True, verbose_name='Адресс')
    terms = models.TextField(max_length=4000, null=True, blank=True, verbose_name='Условия')
    prepayment = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True, verbose_name='Предоплата')
    deposit = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True, verbose_name='Залог')
    cost = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True, verbose_name='Цена')
    max_number_of_people = models.DecimalField(max_digits=10, decimal_places=0, null=True, blank=True,
                                               verbose_name='Максимальное количество участников')
    distance_to_underground = models.DecimalField(max_digits=10, decimal_places=0, null=True, blank=True,
                                                  verbose_name='Расстояние до метро')
    distance_to_railway = models.DecimalField(max_digits=10, decimal_places=0, null=True, blank=True,
                                              verbose_name='Расстояние до электричек')
    status = models.SmallIntegerField(choices=choice_status_list, verbose_name='Статус')
    commentary = models.TextField(max_length=4000, null=True, blank=True)
    creator_id = models.ForeignKey(plat_users, db_column='creator_id', on_delete=models.CASCADE,
                                   verbose_name='Пользователь-создатель')

    class Meta:
        managed = True
        db_table = 'plat_event_location'
        verbose_name = 'Площадка для мероприятия'

from django.conf.urls import url
from django.urls import path
from . import views

# from Event_and_creator.views import Registration_View

app_name = 'Event_and_creator'

urlpatterns = [
    url(r'^main$', views.MainView.as_view()),  # http://127.0.0.1:8000/Event_and_creator/main
    #  url('^register$', Registration_View, name='registration'),  # http://127.0.0.1:8000/Event_and_creator/register/
    # url('^create_user$', views.SignUpView.as_view(), name='create_user')  # http://127.0.0.1:8000/Event_and_creator/create_user
]

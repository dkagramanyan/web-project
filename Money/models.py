from django.db import models
from Event_and_creator.models import plat_event
from People.models import plat_people_participants


# Create your models here.


class plat_income_tickets(models.Model):
    level = models.DecimalField(max_digits=2, decimal_places=0, verbose_name='Уровень билтеа')
    cost = models.DecimalField(max_digits=5, decimal_places=2, verbose_name='Цена')
    event_id = models.ForeignKey(plat_event, db_column='event_id', on_delete=models.CASCADE, verbose_name='Мероприятие')

    class Meta:
        managed = True
        db_table = 'plat_income_tickets'
        verbose_name = 'Билет'
        verbose_name_plural = 'Билеты'


class plat_income_participant_tickets(models.Model):
    ticket_id = models.ForeignKey(plat_income_tickets, db_column='ticket_id',
                                  on_delete=models.CASCADE, verbose_name='Билет')
    participant_id = models.ForeignKey(plat_people_participants, db_column='participant_id',
                                       on_delete=models.CASCADE, verbose_name='Участник')
    event_id = models.ForeignKey(plat_event, db_column='event_id',
                                 on_delete=models.CASCADE, verbose_name='Мероприятие')

    class Meta:
        managed = True
        db_table = 'plat_income_participant_tickets'
        verbose_name = 'Билет участника'
        verbose_name_plural = 'Билеты участника'

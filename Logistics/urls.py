from django.conf.urls import url
from . import views

app_name = 'plat_logistics'

urlpatterns = [
    url(r'^main$', views.MainView.as_view()),  # http://127.0.0.1:8000/Logistics/main
]
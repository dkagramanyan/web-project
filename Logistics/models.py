from django.db import models
from Event_and_creator.models import plat_event, plat_users


# Create your models here.

# логистические компании (вещи + человеки)
class plat_logistic_company(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=40, blank=True, null=True, verbose_name='Название компании')
    website = models.CharField(max_length=40, blank=True, null=True, verbose_name='Вебсайт')
    phone = models.CharField(max_length=15, blank=True, null=True, verbose_name='Номер телефона менеджера')
    cost = models.DecimalField(decimal_places=2, max_digits=10, null=True, blank=True, verbose_name='Стоимость услуги')
    prepayment = models.DecimalField(decimal_places=2, max_digits=10, null=True, blank=True, verbose_name='Предоплата',
                                     default=0)
    deposite = models.DecimalField(decimal_places=2, max_digits=10, null=True, blank=True, verbose_name='Залог',
                                   default=0)
    status = models.DecimalField(decimal_places=0, max_digits=1, verbose_name='Выбран или нет', default=0)
    category_id = models.ForeignKey('plat_logistic_companies_categories', on_delete=models.CASCADE,
                                    verbose_name=' Внешний ключ к категории', db_column='category_id')
    commentary = models.CharField(max_length=4000, blank=True, null=True, verbose_name='Комментарий')
    creator_id = models.ForeignKey(plat_users, on_delete=models.CASCADE,
                                   verbose_name='Внешний ключ к юзеру', db_column='creator_id')

    event_id = models.ForeignKey(plat_event, on_delete=models.CASCADE,
                                 verbose_name='Внешний ключ к мероприятию', db_column='event_id')

    class Meta:
        managed = True
        db_table = 'plat_logistic_company'
        verbose_name = 'Компания'
        verbose_name_plural = 'Компании'


# категории перевозок (вещи, человеки, прочие животные )
class plat_logistic_companies_categories(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=40, verbose_name='Название категории')

    class Meta:
        managed = True
        db_table = 'plat_logistic_companies_categories'
        verbose_name = 'Категория'
        verbose_name_plural = 'Категории'


# предоставляемые атвтобусы
class plat_transfers_buses(models.Model):
    id = models.AutoField(primary_key=True)
    logistic_company_id = models.ForeignKey('plat_logistic_company', on_delete=models.CASCADE,
                                            verbose_name='Автобусы', db_column='logistic_company_id')
    driver_first_name = models.CharField(max_length=20, blank=True, null=True, verbose_name='Имя водителя')
    driver_second_name = models.CharField(max_length=20, blank=True, null=True, verbose_name='Фамилия водителя')
    driver_family_name = models.CharField(max_length=20, blank=True, null=True, verbose_name='Отчество водителя')
    phone = models.CharField(max_length=15, blank=True, null=True, verbose_name='Номер телефона водителя')
    license_plates = models.CharField(max_length=10, blank=True, null=True, verbose_name='Номер автобуса')
    event_id = models.ForeignKey(plat_event, on_delete=models.CASCADE,
                                 verbose_name='Внешний ключ к мероприятию', db_column='event_id')

    class Meta:
        managed = True
        db_table = 'plat_transfers_buses'
        verbose_name = 'Автбус'
        verbose_name_plural = 'Автобусы'


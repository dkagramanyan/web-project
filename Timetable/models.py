from django.db import models
from People.models import plat_people_org, plat_timetable_teams
from Event_and_creator.models import plat_event
from Storage.models import plat_storage_main


# Create your models here.


class plat_timetable_post(models.Model):
    name = models.CharField(max_length=40, verbose_name='Название')
    event_id = models.ForeignKey(plat_event, db_column='event_id', on_delete=models.CASCADE, verbose_name='Мероприятие')

    class Meta:
        managed = True
        db_table = 'plat_timetable_post'
        verbose_name = 'Должность'
        verbose_name_plural = 'Должности'


class plat_timetable_org_posts(models.Model):
    org = models.ForeignKey(plat_people_org, on_delete=models.CASCADE, verbose_name='Организатор')
    post = models.ForeignKey(plat_timetable_post, on_delete=models.CASCADE, verbose_name='Должность')
    event_id = models.ForeignKey(plat_event, db_column='event_id', on_delete=models.CASCADE, verbose_name='Мероприятие')

    def __str__(self):
        return str(self.org) + ' - ' + str(self.post)

    class Meta:
        managed = True
        db_table = 'plat_timetable_org_posts'
        verbose_name = 'Люди по должностям'
        verbose_name_plural = 'Люди по должностям'


class plat_timetable_places(models.Model):
    name = models.CharField(max_length=40, verbose_name='Название')
    features = models.TextField(max_length=40000, blank=True, null=True, verbose_name='Особенности')
    adress = models.CharField(max_length=400, blank=True, null=True, verbose_name='Расположение')
    commentary = models.TextField(max_length=4000, blank=True, null=True, verbose_name='комменатрий')
    event_id = models.ForeignKey(plat_event, db_column='event_id', on_delete=models.CASCADE, verbose_name='Мероприятие')

    def __str__(self):
        return self.name

    class Meta:
        managed = True
        db_table = 'plat_timetable_places'
        verbose_name = 'Место'
        verbose_name_plural = 'Места'


class plat_timetable_cases(models.Model):
    name = models.CharField(max_length=50, verbose_name='Наименование')
    event_id = models.ForeignKey(plat_event, db_column='event_id', on_delete=models.CASCADE, verbose_name='Мероприятие')

    def __str__(self):
        return self.name

    class Meta:
        managed = True
        db_table = 'plat_timetable_cases'
        verbose_name = 'Событие'
        verbose_name_plural = 'События'


class plat_timetable_cases_with_requisite(models.Model):
    case_id = models.ForeignKey(plat_timetable_cases, db_column='case_id', on_delete=models.CASCADE,
                                verbose_name='Событие')
    requisite_id = models.ForeignKey(plat_storage_main, db_column='requisite_id', on_delete=models.CASCADE,
                                     verbose_name='Элемент склада',default=None)
    quantity = models.DecimalField(decimal_places=2, max_digits=10, verbose_name='Количество')
    event_id = models.ForeignKey(plat_event, db_column='event_id', on_delete=models.CASCADE, verbose_name='Мероприятие')

    class Meta:
        managed = True
        db_table = 'plat_timetable_cases_with_requisite'
        verbose_name = 'Событие c реквизитом'
        verbose_name_plural = 'События c реквизитом'


class plat_timetable_main(models.Model):
    case_with_req_id = models.ForeignKey(plat_timetable_cases_with_requisite, blank=True, null=True, db_column='case_with_req_id',
                                         on_delete=models.CASCADE, verbose_name='Событие с реквизитом')
    org_post_id = models.ForeignKey(plat_timetable_org_posts, blank=True, null=True, db_column='org_post_id',
                                    on_delete=models.CASCADE, verbose_name='Человек на должности')
    team_id = models.ForeignKey(plat_timetable_teams, db_column='team_id', null=True, blank=True,
                                on_delete=models.CASCADE, verbose_name='команда')
    place_id = models.ForeignKey(plat_timetable_places, db_column='place_id', on_delete=models.CASCADE,
                                 verbose_name='Локации')
    time_from = models.DateTimeField(verbose_name='Время начала')
    time_to = models.DateTimeField(verbose_name='Время окончания')
    commentary = models.TextField(max_length=4000, null=True, blank=True)
    event_id = models.ForeignKey(plat_event, db_column='event_id', on_delete=models.CASCADE, verbose_name='Мероприятие')

    class Meta:
        managed = True
        db_table = 'plat_timetable_main'
        verbose_name = 'Расписание'
        verbose_name_plural = 'Расписание'

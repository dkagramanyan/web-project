from django.conf.urls import url
from . import views

app_name = 'plat_timetable'

urlpatterns = [
    url(r'^main$', views.MainView.as_view()),  # http://127.0.0.1:8000/timetable/main

]
"""
    Ниже список сторонних ресурсов, использованных для ссылки выше
    http://127.0.0.1:8000/timetable/main  :
    http://timetablejs.org/
"""
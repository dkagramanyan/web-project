from django.shortcuts import render
from django.views.generic import TemplateView


# Create your views here.


class MainView(TemplateView):
    template_name = 'timetable_base.html'


def SchedulerView(request):
    if request.method == 'GET':
        raw_query = "SELECT\
       plat_timetable_main.id,\
       time_from,\
       time_to\
       , plat_timetable_places.name,\
       first_name,\
       middle_name,\
       family_name,\
       vk_url,\
       plat_timetable_post.name,\
       plat_timetable_cases.name\
       FROM\
         plat_timetable_main\
         LEFT OUTER JOIN plat_timetable_places\
         ON plat_timetable_main.place_id = plat_timetable_places.id\
         LEFT OUTER JOIN plat_timetable_cases_with_requisite\
         ON plat_timetable_main.case_with_req_id = plat_timetable_cases_with_requisite.id\
         LEFT OUTER JOIN plat_timetable_teams\
         ON plat_timetable_main.team_id = plat_timetable_teams.id\
         LEFT OUTER JOIN plat_timetable_org_posts\
         ON plat_timetable_main.org_post_id = plat_timetable_org_posts.id\
         LEFT OUTER JOIN plat_people_org\
         ON plat_timetable_org_posts.org_id = plat_people_org.plat_people_main_ptr_id\
         LEFT OUTER JOIN plat_people_main\
         ON plat_people_org.plat_people_main_ptr_id = plat_people_main.id\
         LEFT OUTER JOIN plat_timetable_post\
         ON plat_timetable_org_posts.post_id = plat_timetable_post.id\
         LEFT OUTER JOIN plat_timetable_cases\
         ON plat_timetable_cases_with_requisite.case_id = plat_timetable_cases.id;"
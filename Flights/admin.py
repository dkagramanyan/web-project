from django.contrib import admin
from .models import *
# Register your models here.

admin.site.register(plat_transfers_waybill)
admin.site.register(plat_transfers_stop_points)
admin.site.register(plat_transfers_stop_points_with_waybill)
admin.site.register(plat_transfers_stop_pointsl_with_waybill_with_time)
admin.site.register(plat_transfers_companion)
admin.site.register(plat_transfers_passangers)
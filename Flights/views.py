from django.views.generic.base import TemplateView
from Flights.views_methods import *



def WaybillViewTestIndex(request):
    # стартовая страница трансфера
    return waybill_index(request)

def Waybill_ajax_view(request):
    if request.method == 'POST':
       return waybill_ajax_view_POST(request)

    elif request.method == 'GENERATE':
        return waybill_ajax_view_GENERATE(request)

    elif request.method == 'UPDATE':
        return waybill_ajax_view_UPDATE(request)

    elif request.method == 'DELETE':
        return waybill_ajax_view_DELETE(request)

    elif request.method == 'GET':
        return waybill_ajax_view_GET(request)


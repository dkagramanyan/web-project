from django import forms
from Flights import models
import datetime
from Flights.models import plat_transfers_waybill


class WaybillForm(forms.Form):

    waybill_name = forms.CharField(label='Название путевого листа', max_length=60,
                                   help_text=' Например "трансфер до площадки" ')
    event_id=forms.IntegerField(label='event_id')

    def reset(self):
        self.waybill_name=''
        self.event_id=''


class Meta:
        model = plat_transfers_waybill
        fields = ('name', 'event_id')



"""""""""
class StudentForm(forms.Form):
    SEX_CHOICES = (
        ('m', u"мужской"),
        ('w', u"женский"),
    )
    sex = forms.ChoiceField(label=u'Пол', choices=SEX_CHOICES)
    citizenship = NameModelChoiceField(label=u'Гражданство', queryset=Citizenship.objects.order_by('-name'),
                                       initial=Citizenship.objects.get(id=1))
    doc = forms.CharField(label=u'Документ', max_length=50)
    student_document_type = NameModelChoiceField(label=u'Документ студента',
                                                 queryset=CertificateType.objects.order_by('-name'),
                                                 initial=CertificateType.objects.get(id=1))
    parent_document_type = NameModelChoiceField(label=u'Документ родителей',
                                                queryset=CertificateType.objects.order_by('-name'),
                                                initial=CertificateType.objects.get(id=1))
    event_date = forms.DateTimeField(required=False, label=u'Дата добавления: ', initial=datetime.date.today,
                                     help_text=u'Введите дату')
    fio = forms.CharField(label=u'ФИО студента', max_length=60)
"""
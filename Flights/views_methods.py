from django.views.generic.edit import FormView
from django.views.generic import TemplateView, ListView
from django.http import HttpResponseRedirect
from django.views.generic.base import View
from django.contrib.auth import logout
from django.db import connection
from ast import literal_eval
# from .forms import UserCreateForm
from .serializers import WaybillSerializer
from . import models
from Flights.forms import WaybillForm
from django.shortcuts import render
from django.template.context_processors import csrf

from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import login
from django.http import JsonResponse
from django.http import HttpResponse
# from .serializers import ParticipantSerializer, RoomSerializer, TeamSerializer, TimetableSerializer
from .models import plat_transfers_waybill
import json
from random import randint
from rest_framework.decorators import api_view

def waybill_index(request):
    form = WaybillForm()
    cursor = connection.cursor()
    querry = 'select id, name,event_id from plat_transfers_waybill order by id'
    cursor.execute(querry)
    data = cursor.fetchall()

    new_data = []
    forms_list = {'waybill_form': form}
    for i in range(len(data)):
        dic_element = {'id': data[i][0],
                       'name': data[i][1], 'event_id': data[i][2]}
        new_data.append(dic_element)

    data_to_send = {'forms': forms_list, 'new_data': new_data}

    return render(request, 'transfer.html', data_to_send)



def waybill_ajax_view_POST(request):
    context = {}
    context.update(csrf(request))
    cursor = connection.cursor()

    data = json.loads(request.body.decode('utf8'))
    name = data['waybill_name']
    event_id = data['event_id']
    querry = '''insert into plat_transfers_waybill(name,event_id) 
                        values('%s',%d) ''' \
             % (str(name), int(event_id))
    cursor.execute(querry)
    querry = ''' select id
                       from plat_transfers_waybill
                       where event_id=%d and name='%s' ''' \
             % (int(event_id), str(name))
    cursor.execute(querry)
    data = cursor.fetchall()

    new_data = []

    dic_element = {'id': data[0][0],
                   'name': name, 'event_id': event_id}
    new_data.append(dic_element)
    serializer = WaybillSerializer(new_data, many=True)
    return JsonResponse(serializer.data, safe=False)


def waybill_ajax_view_GENERATE(request):
    context = {}
    context.update(csrf(request))
    cursor = connection.cursor()

    event_id = json.loads(request.body.decode('utf8'))['event_id']
    names = [randint(1, 100) for i in range(5)]

    querry = 'insert into plat_transfers_waybill(event_id,name)' \
             ' values(%d,%s),' \
             '(%d,%s),' \
             '(%d,%s),' \
             '(%d,%s),' \
             '(%d,%s)' \
             % (int(event_id), str(names[0]),
                int(event_id), str(names[1]),
                int(event_id), str(names[2]),
                int(event_id), str(names[3]),
                int(event_id), str(names[4]))
    cursor.execute(querry)
    querry = '''select id
                       from plat_transfers_waybill
                       where (event_id=%d and name='%s') or
                       (event_id=%d and name='%s') or
                       (event_id=%d and name='%s') or
                       (event_id=%d and name='%s') or
                       (event_id=%d and name='%s')  ''' \
             % (
                 int(event_id), str(names[0]),
                 int(event_id), str(names[1]),
                 int(event_id), str(names[2]),
                 int(event_id), str(names[3]),
                 int(event_id), str(names[4]),)
    cursor.execute(querry)
    id_array = cursor.fetchall()
    new_data = []
    for i in range(5):
        dic_element = {'id': id_array[i][0],
                       'name': names[i], 'event_id': event_id}
        new_data.append(dic_element)
    serializer = WaybillSerializer(new_data, many=True)

    return JsonResponse(serializer.data, safe=False)


def waybill_ajax_view_UPDATE(request):
    context = {}
    context.update(csrf(request))
    data = json.loads(request.body.decode('utf8'))
    cursor = connection.cursor()
    querry = ''' update plat_transfers_waybill set name='%s' where id=%d ''' % (
        str(data['waybill_name']), int(data['id_v']))
    cursor.execute(querry)
    print('post_update_done')

    return HttpResponse('update_done')


def waybill_ajax_view_DELETE(request):
    data = json.loads(request.body.decode('utf8'))

    cursor = connection.cursor()
    querry = 'delete from plat_transfers_waybill where id=%s' % (data['id'])
    cursor.execute(querry)
    return HttpResponse('delete_done')


def waybill_ajax_view_GET(request):
    print('get_ajax')
    cursor = connection.cursor()
    querry = 'select id, name,event_id from plat_transfers_waybill order by id'
    cursor.execute(querry)
    data = cursor.fetchall()

    new_data = []

    for i in range(len(data)):
        dic_element = {'id': data[i][0],
                       'name': data[i][1], 'event_id': data[i][2]}
        new_data.append(dic_element)

    serializer = WaybillSerializer(new_data, many=True)

    return JsonResponse(serializer.data, safe=False)

from django.conf.urls import url, include
from django.urls import path
from . import views

app_name = 'Flights'

urlpatterns = [
    url(r'^waybill$',  views.WaybillViewTestIndex, name='waybill'),# http://127.0.0.1:8000/transfer/waybill
    #path('waybill/create_ajax/', views.Waybill_ajax_view, name='waybill_create_ajax'),
    url(r'^waybill/create_ajax/$', views.Waybill_ajax_view, name='waybill_create_ajax')


]

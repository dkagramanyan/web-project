from django.db import models
from Event_and_creator.models import plat_event, plat_users
from People.models import plat_people_org,plat_people_participants
from Logistics.models import plat_transfers_buses
# Create your models here.
'''''
class Person(models.Model):
    # Словарь для поля с выпадающим списком
    choice_status_list = (
        (0, 'Без статуса'),
        (1, 'Организатор'),
        (2, 'Участник'),
    )
    # id человека
    p_id = models.AutoField(primary_key=True)
    # Поле имени
    # max_length - максимальная длинна записи
    # verbose_name - отображаемое имя
    # unique - атрибут для каждой записи уникален
    # blank - возможность атрибута иметь неопределенное значение
    # null -  возможность записать в базу данных неопределенное значение
    # choices - выпадающий список
    middle_name = models.CharField(max_length=20, verbose_name='Фамилия')
    first_name = models.CharField(max_length=20, verbose_name='Имя')
    family_name = models.CharField(max_length=20, verbose_name='Отчество')
    phone = models.CharField(unique=True, max_length=15, verbose_name='Телефон')
    group_name = models.CharField(max_length=10, blank=True, null=True, verbose_name='Номер группы')
    telegram_id = models.CharField(unique=True, max_length=50, verbose_name='Телеграм id')
    vk_url = models.CharField(max_length=50, verbose_name='VK')
    status = models.SmallIntegerField(default=0, verbose_name='Статус', choices=choice_status_list)

    # Вложенный класс определяет связь между моделью и сущностью внутри базы данных
    class Meta:
        managed = True  # изменения модели влекут за собой изменения сущности в базе данных
        db_table = 'ev_people'  # связанная таблица

    # Специальный метод для отображения ФИО целиком
    def i_name(self):
        return self.middle_name + ' ' + self.first_name + ' ' + self.family_name

    # Краткое название метода
    i_name.short_description = 'ФИО'


class Participant(Person):
    choice_status_list_1 = (
        (0, 'Не приехал'),
        (1, 'Приехал'),
        (2, 'Выехал'),
    )
    part_id = models.AutoField(primary_key=True)
    birth_date = models.DateField(blank=True, null=True, verbose_name='Дата Рождения')
    check_in = models.TimeField(blank=True, null=True, verbose_name='Время заезда')
    arrive_status = models.IntegerField(verbose_name='Статус прибытия',
                                        choices=choice_status_list_1, default=0)
    room = models.ForeignKey('Room', on_delete=models.CASCADE, blank=True, null=True, verbose_name='Комната')
    team = models.ForeignKey('Team', on_delete=models.CASCADE, blank=True, null=True, verbose_name='Команда')

    def save(self, *args, **kwargs):
        self.status = 2
        super(Participant, self).save(*args, **kwargs)

    def __str__(self):
        return self.family_name + ' ' + self.first_name + ' ' + self.middle_name

    class Meta:
        managed = True
        db_table = 'ev_part'
        verbose_name = 'Участник'
        verbose_name_plural = 'Участники'


class Org(Person):
    org_id = models.AutoField(primary_key=True)

    def save(self, *args, **kwargs):
        self.status = 1
        super(Org, self).save(*args, **kwargs)

    def __str__(self):
        return self.middle_name + ' ' + self.family_name + ' ' + self.first_name

    class Meta:
        managed = True
        db_table = 'ev_org'
        verbose_name = 'Организатор'
        verbose_name_plural = 'Организаторы'
'''
#########################################################################################################






# автобусные рейсы с датой
class plat_transfers_bus_flights(models.Model):
    id = models.AutoField(primary_key=True)
    transfer_id = models.ForeignKey(plat_transfers_buses, on_delete=models.CASCADE,
                                    verbose_name=' Внешний ключ к автобусам',db_column='transfer_id')
    day = models.DateField(blank=True, null=True, verbose_name='Дата  рейса')
    event_id = models.ForeignKey(plat_event, on_delete=models.CASCADE,
                                            verbose_name='Внешний ключ к мероприятию',db_column='event_id')

    class Meta:
        managed = True
        db_table = 'plat_transfers_bus_flights'
        verbose_name = 'Рейс'
        verbose_name_plural = 'Рейсы'


#точки остановок на маршруте
class plat_transfers_stop_points(models.Model):
    id = models.AutoField(primary_key=True)
    adress = models.CharField(max_length=20, blank=True, null=True, verbose_name='Адрес точки')
    commentary = models.CharField(max_length=20, blank=True, null=True, verbose_name='Комментарий')
    event_id = models.ForeignKey(plat_event, on_delete=models.CASCADE,
                                 verbose_name='Внешний ключ к мероприятию', db_column='event_id')

    class Meta:
        managed = True
        db_table = 'plat_transfers_stop_points'
        verbose_name = 'Точка на маршруте'
        verbose_name_plural = 'точки на маршруте'


#путевые листы
class plat_transfers_waybill(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=40, verbose_name='Название путевого листа')
    event_id = models.ForeignKey(plat_event, on_delete=models.CASCADE,
                                 verbose_name='Внешний ключ к мероприятию', db_column='event_id')

    class Meta:
        managed = True
        db_table = 'plat_transfers_waybill'
        verbose_name = 'Путевой лист'
        verbose_name_plural = 'Путевые листы'

    def __str__(self):
        return self.name


#Точки остановок на маршруте в путевом листе
class plat_transfers_stop_points_with_waybill(models.Model):
    id = models.AutoField(primary_key=True)
    waybill_id = models.ForeignKey('plat_transfers_waybill', on_delete=models.CASCADE,
                                            verbose_name='Внешний ключ к путевому листу', db_column='waybill_id')
    stop_point_id = models.ForeignKey('plat_transfers_stop_points', on_delete=models.CASCADE,
                                            verbose_name='Внешний ключ к точки остановки', db_column='stop_point_id')
    point_gradation_number = models.DecimalField(decimal_places=0, max_digits=5, null=True, blank=True,
                                                 verbose_name='Номер точки остановки в путевом листе')
    event_id = models.ForeignKey(plat_event, on_delete=models.CASCADE,
                                 verbose_name='Внешний ключ к мероприятию', db_column='event_id')

    class Meta:
        managed = True
        db_table = 'plat_transfers_stop_points_with_waybill'
        verbose_name = 'Точка на путевом листе'
        verbose_name_plural = 'Точки на путевом листе'


#Точки остановок на маршруте в путевом листе со временем
class plat_transfers_stop_pointsl_with_waybill_with_time(models.Model):
    id = models.AutoField(primary_key=True)
    waybill_with_points_id = models.ForeignKey('plat_transfers_stop_points_with_waybill', on_delete=models.CASCADE,
                                            verbose_name='Внешний ключ к рейсам', db_column=' waybill_with_points_id')
    time_theorethical  = models.TimeField(blank=True, null=True, verbose_name='Время прибытия в точку в теории')
    time_by_map  = models.TimeField(blank=True, null=True, verbose_name='Время прибытия в точку на практике')
    flights_id = models.ForeignKey('plat_transfers_bus_flights', on_delete=models.CASCADE,
                                            verbose_name='Внешний ключ к рейсам',db_column='flights_id')
    event_id = models.ForeignKey(plat_event, on_delete=models.CASCADE,
                                 verbose_name='Внешний ключ к мероприятию', db_column='event_id')

    class Meta:
        managed = True
        db_table = 'plat_transfers_stop_pointsl_with_waybill_with_time'
        verbose_name = 'Точка остановки на маршруте в путевом листе со временем'
        verbose_name_plural = 'Точки остановок на маршруте в путевом листе со временем'


class plat_transfers_passangers(models.Model):
    id = models.AutoField(primary_key=True)
    transfer_id = models.ForeignKey('plat_transfers_bus_flights', on_delete=models.CASCADE,
                                            verbose_name='Внешний ключ к рейсу',db_column='transfer_id')
    participant_id = models.ForeignKey(plat_people_participants, on_delete=models.CASCADE,
                                            verbose_name='Внешний ключ к участнику',db_column='participant_id')
    event_id = models.ForeignKey(plat_event, on_delete=models.CASCADE,
                                 verbose_name='Внешний ключ к мероприятию', db_column='event_id')

    class Meta:
        managed = True
        db_table = 'plat_transfers_passangers'
        verbose_name = 'Участник в рейсе'
        verbose_name_plural = 'Участники в рейсе'

class plat_transfers_companion(models.Model):
    id = models.AutoField(primary_key=True)
    transfer_id = models.ForeignKey('plat_transfers_bus_flights', on_delete=models.CASCADE,
                                            verbose_name='Внешний ключ к рейсам',db_column='transfer_id')
    org_id = models.ForeignKey(plat_people_org, on_delete=models.CASCADE,
                                            verbose_name='Внешний ключ к организатору',db_column='org_id')
    event_id = models.ForeignKey(plat_event, on_delete=models.CASCADE,
                                 verbose_name='Внешний ключ к мероприятию', db_column='event_id')

    class Meta:
        managed = True
        db_table = 'plat_transfers_companion'
        verbose_name = 'Организатор в рейсе'
        verbose_name_plural = 'Организаторы в рейсе'

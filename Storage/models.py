from django.db import models
from Event_and_creator.models import plat_event, plat_users
from People.models import plat_people_org



# Единицы измерения
class plat_storage_units(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=40, verbose_name='Название единицы измерения')
    short_name = models.CharField(max_length=10, verbose_name='Короткая форма записи')

    class Meta:
        managed = True
        db_table = 'plat_storage_units'
        verbose_name = 'Единица измерения'
        verbose_name_plural = 'Единицы измерения'


# Базовые элементы склада
class plat_storage_base_names(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=40, verbose_name='Название базового элемента')
    creator_id = models.ForeignKey(plat_users, db_column='creator_id', on_delete=models.CASCADE,
                                   verbose_name='Пользователь-создатель')

    class Meta:
        managed = True
        db_table = 'plat_storage_base_names'
        verbose_name = 'Базовый элемент склада'
        verbose_name_plural = 'Базовые элементы склада'


# Базовые категории склада
class plat_storage_base_category(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=40, verbose_name='Название базовой категории')
    creator_id = models.ForeignKey(plat_users, db_column='creator_id', on_delete=models.CASCADE,
                                   verbose_name='Пользователь-создатель')

    class Meta:
        managed = True
        db_table = 'plat_storage_base_category'
        verbose_name = 'Базовая категория склада'
        verbose_name_plural = 'Базовые категории склада'


# Склад
class plat_storage_main(models.Model):
    choice_status_list = (
        (0, 'Нет'),
        (1, 'Да'),
    )
    id = models.AutoField(primary_key=True)
    name_id = models.ForeignKey('plat_storage_base_names', db_column='name_id', on_delete=models.CASCADE,
                                verbose_name='Внешний ключ к базовому складу')
    amount = models.DecimalField(decimal_places=2, max_digits=5, null=True, blank=True,
                                 verbose_name='Количество')
    units_id = models.ForeignKey('plat_storage_units', db_column='units_id', on_delete=models.CASCADE,
                                 verbose_name='Внешний ключ к базовому складу')
    complex = models.SmallIntegerField(verbose_name='Сложный или нет', default=0, choices=choice_status_list)
    category_id = models.ForeignKey('plat_storage_base_category', db_column=' category_id', on_delete=models.CASCADE,
                                    verbose_name='Внешний ключ к категории склада')
    commentary = models.CharField(max_length=4000, verbose_name='Комментарий')
    event_id = models.ForeignKey(plat_event, on_delete=models.CASCADE,
                                 verbose_name='Внешний ключ к мероприятию', db_column='event_id')
    class Meta:
        managed = True
        db_table = 'plat_storage_main'
        verbose_name = 'Элемент склада'
        verbose_name_plural = 'Элементы склада'

"""
# DIY реквизит
class plat_stoage_diy_requisite(models.Model):
    id = models.AutoField(primary_key=True)
    parent_id = models.ForeignKey('plat_storage_main', db_column='parent_id', on_delete=models.CASCADE,
                                  verbose_name='Внешний ключ к складу')
    child_id = models.ForeignKey('plat_storage_main', db_column='child_id', on_delete=models.CASCADE,
                                 verbose_name='Внешний ключ к складу')
    parts_number = models.DecimalField(max_digits=5, verbose_name='Количество используемых единиц')

    class Meta:
        managed = True
        db_table = 'plat_stoage_diy_requisite'
        verbose_name = 'DIY реквизит'
        verbose_name_plural = 'DIY реквизит'

"""
# Адреса складов
class plat_storage_address(models.Model):
    id = models.AutoField(primary_key=True)
    adress = models.CharField(max_length=40, verbose_name='Адрес склада')
    commentary = models.CharField(max_length=4000, verbose_name='Комментарий')
    event_id = models.ForeignKey(plat_event, on_delete=models.CASCADE,
                                 verbose_name='Внешний ключ к мероприятию', db_column='event_id')

    class Meta:
        managed = True
        db_table = 'plat_storage_address'
        verbose_name = 'Адрес склада'
        verbose_name_plural = 'Адреса складов'

#Поставщики
class plat_storage_providers(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=40, verbose_name='Имя поставщика')
    website = models.CharField(max_length=100, verbose_name='Сайт ')
    phone = models.CharField(max_length=15, verbose_name='Номер телефона поставщика')
    commentary = models.CharField(max_length=4000, verbose_name='Комментарий')
    creator_id = models.ForeignKey(plat_users, db_column='creator_id', on_delete=models.CASCADE,
                                   verbose_name='Пользователь-создатель')
    event_id = models.ForeignKey(plat_event, on_delete=models.CASCADE,
                                 verbose_name='Внешний ключ к мероприятию', db_column='event_id')

    class Meta:
        managed = True
        db_table = 'plat_storage_providers'
        verbose_name = 'Поставщик'
        verbose_name_plural = 'Поставщики'

#Адреса поставщиков
class plat_storage_providers_adress(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=40, verbose_name='Адрес поставщика')
    provider_id = models.ForeignKey('plat_storage_providers', on_delete=models.CASCADE,
                                 verbose_name='Внешний ключ к поставщику', db_column='provider_id')

    class Meta:
        managed = True
        db_table = 'plat_storage_providers_adress'
        verbose_name = 'Адрес поставщиков'
        verbose_name_plural = 'Адреса поставщиков'

#Характеристика спонсора
class plat_storage_sponsor(models.Model):
    id = models.AutoField(primary_key=True)
    provider_id = models.ForeignKey('plat_storage_providers', on_delete=models.CASCADE,
                                    verbose_name='Внешний ключ к поставщику', db_column='provider_id')

    compensation = models.DecimalField(max_digits=10, decimal_places=2,verbose_name='Рзмер компенсации')
    manager_first_name = models.CharField(max_length=20, verbose_name='Имя менеджера')
    manager_second_name = models.CharField(max_length=20, verbose_name='Фамилия менеджера')
    manager_family_name = models.CharField(max_length=20, verbose_name='Отчество менеджера')
    terms = models.CharField(max_length=4000, verbose_name='Адрес поставщика')
    commentary = models.CharField(max_length=4000, verbose_name='Адрес поставщика')
    status = models.DecimalField(max_digits=1,decimal_places=0,verbose_name='Выбран или нет')

    class Meta:
        managed = True
        db_table = 'plat_storage_sponsor'
        verbose_name = 'Характеристика спонсоров'
        verbose_name_plural = 'Характеристики спонсоров'

#Логистика элементов склада
class plat_storage_base_logistics(models.Model):
    id = models.AutoField(primary_key=True)
    amount_to_transport = models.DecimalField(max_digits=5, decimal_places=2,verbose_name='Количество товара на отгрузку')
    description = models.CharField(max_length=4000, verbose_name='Описание')
    owner_id = models.ForeignKey(plat_people_org, on_delete=models.CASCADE,
                                 verbose_name='Внешний ключ к оргу', db_column=' owner_id')

    carried_by = models.IntegerField(max_length=40, verbose_name='кто везет')
    adress_id = models.ForeignKey('plat_storage_address', on_delete=models.CASCADE,
                                 verbose_name='Внешний ключ к адресу склада', db_column='adress_id')

    provider_id = models.ForeignKey('plat_storage_providers', on_delete=models.CASCADE,
                                 verbose_name='Внешний ключ к поставщику', db_column='provider_id')

    provider_adress_id = models.ForeignKey('plat_storage_providers_adress', on_delete=models.CASCADE,
                                 verbose_name='Внешний ключ к дресу поставщика', db_column='provider_adress_id')

    is_bought = models.DecimalField(decimal_places=0, max_digits=1,verbose_name='Покупается или нет')
    website = models.CharField(max_length=4000, verbose_name='Вебсайт')
    time = models.DateTimeField(verbose_name='Дата и время выдачи')
    price_per_piece = models.DecimalField(max_digits=5,decimal_places=2,verbose_name='Стоимость за единицу')
    event_id = models.ForeignKey(plat_event, on_delete=models.CASCADE,
                                 verbose_name='Внешний ключ к мероприятию', db_column='event_id')

    class Meta:
        managed = True
        db_table = 'plat_storage_base_logistics'
        verbose_name = 'Логистика элементов склада'
        verbose_name_plural = 'Логистика элементов склада'


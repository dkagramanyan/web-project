
    $('#savebutton').click(function () {
        var name_input = $('input[name="waybill_name"]').val();
        var event_id_input = $('input[name="event_id"]').val();

        $.ajax({
            type: "POST",
            url: '/transfer/waybill/create_ajax/',
            data: JSON.stringify({
                waybill_name: name_input,
                event_id: event_id_input
            }),
            contentType: 'application/json; charset=utf-8',
            success: function (data) {
                document.getElementsByName('WaybillForm')[0].reset();
                append_waybill(data[0]);


            },
            error: function (data) {
                alert('error_post')
            }
        });


    });

    $('#updatebutton').click(function () {

        $.ajax({
            url: '/transfer/waybill/create_ajax/',
            dataType: 'json',
            type: 'GET',
            safe: false,
            success: function (data) {
                table = document.getElementById("WaybillTable");

                rows_delete = table.rows.length;

                for (i = rows_delete - 1; i > 0; i--) {
                    table.deleteRow(i);
                }
                rows_len = Object.keys(data).length;// количество строк

                for (i = 0; i < rows_len; i++) {
                    append_waybill(data[i])
                }


            }

        });


    });

    $("#edit_form_save").click(function () {

        var id_v = edit_name.getAttribute('waybill_id');
        var name_input = $('input[name="edit_name"]').val();
        var event_id_input = $('input[name="edit_event_id"]').val();


        $.ajax({
            type: "UPDATE",
            url: '/transfer/waybill/create_ajax/',
            data: JSON.stringify({
                id_v: id_v,
                waybill_name: name_input,
                event_id: event_id_input
            }),
            contentType: 'application/json; charset=utf-8',
            success: function (data_to_send) {
                waybill_update(id_v, name_input, event_id_input);

            },
            error: function (data) {
                alert('error_update');

            }
        });
        $('form#update_waybill').trigger("reset");
        $('#myModal').modal('hide');
        return false;
    });

    function delete_waybill(id_v) {
        var action = confirm("Точно хочешь удалить ? вернуть нельзя будет (пока что)");
        if (action) {
            $.ajax({
                url: '/transfer/waybill/create_ajax/',
                data: JSON.stringify({
                    id: id_v
                }),
                type: 'DELETE',
                contentType: 'application/json; charset=utf-8',
                success: function (data) {

                    $("#waybill-" + id_v).remove();
                },
                error: function (data) {
                    alert('delete_error')

                }
            });
        }


    }

    // генерит 5 записей
    $('#generatebutton').click(function () {

        var name_input = $('input[name="waybill_name"]').val();
        var event_id_input = $('input[name="event_id"]').val();

        $.ajax({
            type: "GENERATE",
            url: '/transfer/waybill/create_ajax/',
            data: JSON.stringify({
                waybill_name: name_input,
                event_id: event_id_input
            }),
            success: function (data) {
                document.getElementsByName('WaybillForm')[0].reset();
                rows_len = Object.keys(data).length;// количество строк
                for (i = 0; i < rows_len; i++) {
                    append_waybill(data[i])
                }
            },
            error: function (data) {
                alert('error_generate\n event_id забыл, долбаеб')
            }
        });


    });


    // запонляет форму редактиврования
    function editUser(id) {
        if (id) {

            tr_id = "#waybill-" + id;
            name = $(tr_id).find(".waybill_name").text();       //таблица
            event_id = $(tr_id).find(".waybill_event_id").text();
            $('#edit_name').val(name);                          //форма
            $('#edit_event_id').val(event_id);
            edit_name.setAttribute('waybill_id', id);
        }
    }

    // еще не работает эта фцнкция
    //изменяет значение записи при редактировании
    function waybill_update(id_v, name_input, event_id_input) {
              $("#WaybillTable #waybill-" + id_v).children(".userData").each(function () {
            var attr = $(this).attr("name");
            if (attr === "name") {
                $(this).text(name_input);

            }
        });
    }


    function append_waybill(data) {
        $("#WaybillTable > tbody:last-child").append(`  <tr id="waybill-${data.id}">
            <td class="id" name="id">${data.id}</td>
            <td class="waybill_name userData" name="name">${data.name}</td>
            <td class="waybill_event_id userData" name="event_id">${data.event_id}</td>

            <td align="center">
                <button class="btn btn-success form-control" onClick="editUser(${data.id})"
                data-toggle="modal" data-target="#myModal")">Редактировать</button>
            </td>
            <td align="center">
                <button class="btn btn-danger form-control" onClick="delete_waybill(${data.id})">Удалить</button>
            </td>
        </tr> `);

    }

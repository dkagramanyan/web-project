from django.views.generic.base import TemplateView
from Flights.views_methods import *


class MainView(TemplateView):
    # основная страница проекта
    template_name = 'Main_page.html'

class SecretView(TemplateView):
    template_name = 'secret.html'

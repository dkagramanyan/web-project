"""Events URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path
from django.conf.urls import url
from Event_and_creator import urls

from django.contrib import admin
from django.urls import include
from . import views

urlpatterns = [
    url(r'^event_and_creator/', include('Event_and_creator.urls')),  # http://127.0.0.1:8000/event_and_creator/main
    url(r'^people/', include('People.urls')),  # http://127.0.0.1:8000/people/main
    url(r'^transfer/', include('Flights.urls')),  # http://127.0.0.1:8000/transfer/main
    url(r'^logistics/', include('Logistics.urls')),  # http://127.0.0.1:8000/logistics/main
    url(r'^money/', include('Money.urls')),  # http://127.0.0.1:8000/money/main
    url(r'^rooms/', include('Rooms.urls')),  # http://127.0.0.1:8000/rooms/main
    url(r'^storage/', include('Storage.urls')),  # http://127.0.0.1:8000/storage/main
    url(r'^timetable/', include('Timetable.urls')),  # http://127.0.0.1:8000/timetable/main
    path('admin/', admin.site.urls),
    url(r'^main$', views.MainView.as_view()),  # http://127.0.0.1:8000/main
    url(r'^secret$', views.SecretView.as_view()),  # http://127.0.0.1:8000/secret
]
